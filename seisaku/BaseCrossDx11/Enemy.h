#pragma once
#include "stdafx.h"

namespace basedx11 {

	class Scene;
	class GameObject;

	class Enemy : public GameObject {
		shared_ptr< StateMachine<Enemy> >  m_StateMachine;	//ステートマシーン

															//バッファ関連
		ComPtr<ID3D11Buffer> m_VertexBuffer;	///<頂点バッファ
		ComPtr<ID3D11Buffer> m_IndexBuffer;	///<インデックスバッファ
		UINT m_NumVertices;				///<頂点の数
		UINT m_NumIndicis;				///<インデックスの数
		UINT m_Division;				///<分割数
		wstring m_TextureFileName;		///<テクスチャファイル名
		shared_ptr<TextureResource> m_TextureResource;	///<テクスチャリソース
		Vector3 m_Scale;				///<スケーリング
		Quaternion m_Qt;			///<回転
		Vector3 m_Pos;				///<位置
		bool m_Trace;					///<透明処理するかどうか
		void CreateBuffers();

		float m_MoveXDir;	//X移動方向

	public:
		//--------------------------------------------------------------------------------------
		/*!
		@brief コンストラクタ
		@param[in]	PtrScene	シーンのポインタ
		@param[in]	Division	分割数
		@param[in]	TextureFileName	テクスチャファイル名
		@param[in]	Trace	透明処理するかどうか
		@param[in]	Pos	位置
		*/
		//--------------------------------------------------------------------------------------
		Enemy(const shared_ptr<Scene> PtrScene,
			UINT Division, const wstring& TextureFileName, bool Trace, const Vector3& Pos);
		//--------------------------------------------------------------------------------------
		/*!
		@brief デストラクタ
		*/
		//--------------------------------------------------------------------------------------
		virtual ~Enemy();

		//--------------------------------------------------------------------------------------
		/*!
		@brief ステートマシンの取得
		@return	ステートマシン
		*/
		//--------------------------------------------------------------------------------------
		shared_ptr< StateMachine<Enemy> > GetStateMachine() const {
			return m_StateMachine;
		}

		//--------------------------------------------------------------------------------------
		/*!
		@brief 初期化
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnCreate() override;
		//--------------------------------------------------------------------------------------
		/*!
		@brief 更新
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		virtual void OnUpdate()override;


		//--------------------------------------------------------------------------------------
		/*!
		@brief アクションを実行する
		@param[in]	motion	アクション
		@return	なし
		*/
		//--------------------------------------------------------------------------------------
		void SetActionMotion(EnemyMotion motion);

		//--------------------------------------------------------------------------------------
		/*!
		@brief 現在のアクションが終了したかどうか
		@return	終了していればtrue
		*/
		//--------------------------------------------------------------------------------------
		bool IstActionArrived();


	};


	//--------------------------------------------------------------------------------------
	//	class RightState : public ObjState<Enemy>;
	//	用途: 右向き移動
	//--------------------------------------------------------------------------------------
	class RightState : public ObjState<Enemy>
	{
		RightState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<RightState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class LeftState : public ObjState<Enemy>;
	//	用途: 左向き移動
	//--------------------------------------------------------------------------------------
	class LeftState : public ObjState<Enemy>
	{
		LeftState() {}
	public:
		//ステートのインスタンス取得
		static shared_ptr<LeftState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Enemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Enemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Enemy>& Obj)override;
	};



}
//end basedx11
