/*!
@file GameStage.cpp
@brief ゲームステージ実体
*/

#include "stdafx.h"
#include "Project.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス実体
	//--------------------------------------------------------------------------------------


	//リソースの作成
	void GameStage::CreateResourses() {
		wstring DataDir;
		App::GetApp()->GetDataDirectory(DataDir);
		wstring strTexture = DataDir + L"trace.png";
		App::GetApp()->RegisterTexture(L"TRACE_TX", strTexture);
		strTexture = DataDir + L"dart.jpg";
		App::GetApp()->RegisterTexture(L"SKY_TX", strTexture);
		strTexture = DataDir + L"Blue.jpg";
		App::GetApp()->RegisterTexture(L"WALL_TX", strTexture);
		strTexture = DataDir + L"spark.png";
		App::GetApp()->RegisterTexture(L"SPARK_TX", strTexture);
		strTexture = DataDir + L"number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Sunset Sarsaparilla.jpg";
		App::GetApp()->RegisterTexture(L"SS_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Black.png";
		App::GetApp()->RegisterTexture(L"BLK_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Blue.png";
		App::GetApp()->RegisterTexture(L"BLU_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"White.png";
		App::GetApp()->RegisterTexture(L"WHI_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Red.png";
		App::GetApp()->RegisterTexture(L"RED_TX", strTexture);

	}



	//ビューとライトの作成
	void GameStage::CreateViewLight() {
		auto PtrView = CreateView<SingleView>();
		//ビューのカメラの設定
		auto PtrLookAtCamera = ObjectFactory::Create<Camera>();
		PtrView->SetCamera(PtrLookAtCamera);
		PtrLookAtCamera->SetEye(Vector3(0.0f, 15.0f, -2.0f));
		PtrLookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
		//シングルライトの作成
		auto PtrSingleLight = CreateLight<SingleLight>();
		//ライトの設定
		PtrSingleLight->GetLight().SetPositionToDirectional(-0.25f, 1.0f, -0.25f);
	}


	//プレートの作成
	void GameStage::CreatePlate() {
		//ステージへのゲームオブジェクトの追加
		auto Ptr = AddGameObject<GameObject>();
		auto PtrTrans = Ptr->GetComponent<Transform>();
		Quaternion Qt;
		Qt.RotationRollPitchYawFromVector(Vector3(XM_PIDIV2, 0, 0));
		Matrix4X4 WorldMat;
		WorldMat.DefTransformation(
			Vector3(200.0f, 200.0f, 1.0f),
			Qt,
			Vector3(0.0f, 0.0f, 0.0f)
		);
		PtrTrans->SetScale(200.0f, 200.0f, 1.0f);
		PtrTrans->SetQuaternion(Qt);
		PtrTrans->SetPosition(0.0f, 0.0f, 0.0f);

		////描画コンポーネントの追加
		//auto DrawComp = Ptr->AddComponent<PNTStaticDraw>();
		////描画コンポーネントに形状（メッシュ）を設定
		//DrawComp->SetTextureResource(L"BLK_TX");
	 //  //自分に影が映りこむようにする
  //  	/*DrawComp->SetOwnShadowActive(true);*/
		//
		////DrawComp->SetMeshResource(L"DEFAULT_SQUARE");
		//////自分に影が映りこむようにする
		////DrawComp->SetOwnShadowActive(true);

		////描画コンポーネントテクスチャの設定
		//DrawComp->SetTextureResource(L"BLK_TX");
	
	
	}



	//追いかけるオブジェクトの作成
	void GameStage::CreateSeekObject() {
		//オブジェクトのグループを作成する
		auto Group = CreateSharedObjectGroup(L"ObjectGroup");
		//配列の初期化
		vector<Vector3> Vec = {
			{ 0, 0.125f, 10.0f },
			{ 10.0f, 0.125f, 0.0f },
			{ -10.0f, 0.125f, 0.0f },
			{ 0, 0.125f, -10.0f },
		};


	}



	//固定のボックスの作成(ステージ)
	void GameStage::CreateFixedBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			//上
			{
				Vector3(6.0f, 0.5f, 0.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0, 0.25f, 4.25f)
			},
			//下
			{
				Vector3(6.0f, 0.5f, 0.5f),
				Vector3(0.0f, 0.0f, 0.0f),
				Vector3(0, 0.25f, -2.25f)
			},
			//右
			{
				Vector3(0.5f, 0.5f, 6.0f),
				Vector3(0, 0, 0),
				Vector3(2.75f, 0.25f, 1.0f)
			},
			//左
			{
				Vector3(0.5f, 0.5f, 6.0f),
				Vector3(0, 0, 0),
				Vector3(-2.75f, 0.25f, 1.0f)
			},
			//障害物1
			{
				Vector3(3.0f, 0.5f, 0.5f),
				Vector3(0, 0, 0),
				Vector3(-1.0f, 0.25f, 0.25f)
			},
			//障害物2
			{
				Vector3(3.0f, 0.5f, 0.5f),
				Vector3(0, 0, 0),
				Vector3(1.0f, 0.25f, 1.25f)
			},
			//障害物2
		{
			Vector3(0.5f, 0.5f, 1.0f),
				Vector3(0, 0, 0),
				Vector3(-0.9f, 0.25f, -0.5f)
		}
		};

		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<FixedBox>(v[0], v[1], v[2]);
		}
	}

	void GameStage::CreateGoalBox() {
		//配列の初期化
		vector< vector<Vector3> > Vec = {
			//ゴール
			{
				Vector3(1.0f, 0.5f, 0.5f),
				Vector3(0, 0, 0),
				Vector3(-2.0f, 0.25f, 3.75f)
			}
		};
		//オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<GoalBox>(v[0], v[1], v[2]);
		}
	}

	//プレイヤーの作成
	void GameStage::CreatePlayer() {
		//プレーヤーの作成
		auto PlayerPtr = AddGameObject<Player>();
		//シェア配列にプレイヤーを追加
		SetSharedGameObject(L"Player", PlayerPtr);
	}

	//左右移動しているボックスの作成
	void GameStage::CreateMoveBox() {
		CreateSharedObjectGroup(L"MoveBox");
		AddGameObject<MoveBox>(
			Vector3(0.5f, 0.5f, 0.5f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(-1.75f, 0.25f, 0.75f)
			);
	}


	//ヒットする球体の作成
	void GameStage::CreateSphere() {
		//配列の初期化
		vector<Vector3> Vec = {
			{ 20.0f, 0, 25.0f },
			{ 20.0f, 0, 0.0f },
		};
		//配置オブジェクトの作成
		for (auto v : Vec) {
			AddGameObject<SphereObject>(v);
		}
	}

	//半透明のスプライト作成
	//void GameStage::CreateTraceSprite() {
	//	AddGameObject<TraceSprite>(true,
	//		Vector2(200.0f, 200.0f), Vector2(-500.0f, -280.0f));
	//}


	//壁模様のスプライト作成
	void GameStage::CreateWallSprite() {


		auto spg = CreateSharedObjectGroup(L"SelectPlateGroup");

		//メインテキスト
		auto SelectPlate = AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(400.0f, 100.0f), Vector2(-300.0f, 280.0f));
		SetSharedGameObject(L"Mainplate", SelectPlate);


		//上テキスト
		SelectPlate = AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(1.0f, 5.0f), Vector2(-200.0f, 130.0f));
		spg->IntoGroup(SelectPlate);

		//左テキスト
		SelectPlate = AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(1.0f, 5.0f), Vector2(-500.0f, -40.0f));
		spg->IntoGroup(SelectPlate);

		//右テキスト
		SelectPlate = AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(1.0f, 5.0f), Vector2(100.0f, -40.0f));
		spg->IntoGroup(SelectPlate);

		//下テキスト
		SelectPlate = AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(1.0f, 5.0f), Vector2(-200.0f, -200.0f));
		spg->IntoGroup(SelectPlate);

		SelectPlate = AddGameObject<WallSprite>(L"WALL_TX", false,
			Vector2(1.0f, 5.0f), Vector2(200.0f, -200.0f));
		spg->IntoGroup(SelectPlate);
	}

	////スクロールするスプライト作成
	//void GameStage::CreateScrollSprite() {
	//	AddGameObject<ScrollSprite>(L"TRACE_TX",true,
	//		Vector2(160.0f, 40.0f),Vector2(500.0f,-280.0f));
	//}

	//スパークの作成
	void GameStage::CreateSpark() {
		auto MultiSparkPtr = AddGameObject<MultiSpark>();
		//シェア配列にスパークを追加
		SetSharedGameObject(L"MultiSpark", MultiSparkPtr);
		//エフェクトはZバッファを使用する
		GetParticleManager()->SetZBufferUse(true);
	}




	void GameStage::OnCreate() {
		try {
			//リソースの作成
			CreateResourses();
			//ビューとライトの作成
			CreateViewLight();
			//プレートの作成
			CreatePlate();
			//固定のボックスの作成
			CreateFixedBox();
			//上下移動しているボックスの作成
			CreateMoveBox();
			//球体作成
			CreateSphere();
			//追いかけるオブジェクトの作成
			CreateSeekObject();
			//半透明のスプライト作成
			/*CreateTraceSprite();*/
			//ゴールのボックスの作成
			CreateGoalBox();
			//壁模様のスプライト作成
			CreateWallSprite();
			////スクロールするスプライト作成
			//CreateScrollSprite();
			//スパークの作成
			CreateSpark();
			//プレーヤーの作成
			CreatePlayer();
		}
		catch (...) {
			throw;
		}
	}

	void GameStage::OnUpdate()
	{
		auto Select = GetSharedObjectGroup(L"SelectPlateGroup");
		//	
		//	for (int i = 0; i < Select->size(); i++) 
		//	{
		//		Select->at(i)->AddComponent<Action>()->AddScaleBy(1,Vector3(1.0f,1.0f,0.0f));
		//		Select->at(i)->AddComponent<Action>()->AddScaleBy(1, Vector3(-1.0f, -1.0f, 0.0f));

		//		Select->at(i)->AddComponent<Action>()->SetLooped(true);

		//		Select->at(i)->AddComponent<Action>()->Run();

		//	}

		//カーソルを選択する処理
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (!move_flg) {
			if (CntlVec[0].bConnected) {
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_UP) {
					selectNum = 0;
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_LEFT) {
					selectNum = 1;
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_RIGHT) {
					selectNum = 2;
				}
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_DPAD_DOWN) {
					selectNum = 3;
				}

				for (int i = 0; i < Select->size(); i++)
				{
					auto sentaku = Select->at(i)->GetComponent<Transform>();
					if (i == selectNum) {

						if (sentaku->GetScale().x <= 100) {
							flgaaaaaa = true;
						}
						else if (sentaku->GetScale().x >= 200) {
							flgaaaaaa = false;
						}
						continue;
					}
					sentaku->SetScale(70, 50, 0.1);
				}

				if (CntlVec[0].wPressedButtons) {
					if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
						Move_Storage[MS_Counter] = selectNum;
						MS_Counter++;
					}

				}
				if (MS_Counter == 3) {
					MS_Counter = 0;
					for (int i = 0; i < Select->size(); i++) {
						Select->at(i)->SetDrawActive(false);
					}
					GetSharedGameObject<WallSprite>(L"Mainplate")->SetDrawActive(false);
					move_flg = true;
					auto player = GetSharedGameObject<Player>(L"Player");
					auto act = player->GetComponent<Action>();
					act->AllActionClear();
					/*player->GetComponent<Action>()->Run();*/
				}
			}

			auto SLTrans = Select->at(selectNum)->GetComponent<Transform>();
			float selectSpeed = 3.0f;

			if (!flgaaaaaa) {
				SLTrans->SetScale(SLTrans->GetScale().x - selectSpeed, SLTrans->GetScale().y - selectSpeed / 2, SLTrans->GetScale().z);
			}
			else {
				SLTrans->SetScale(SLTrans->GetScale().x + selectSpeed, SLTrans->GetScale().y + selectSpeed / 2, SLTrans->GetScale().z);
			}
		}
		//選択したときの処理
		else if (move_flg) {
			moveTimer = 0;
			auto player = GetSharedGameObject<Player>(L"Player");
			auto player_pos = player->GetComponent<Transform>()->GetPosition();

			/*for (int i = 0; i < 3; i++) {
				//if (Move_Storage[3-i] == 0) {
				if(Move_Storage[i] == 0){
					player->GetComponent<Transform>()->SetPosition(player_pos.x, player_pos.y, player_pos.z + 1);
				}
				//else if (Move_Storage[3-i] == 1) {
				else if (Move_Storage[i] == 1) {
					player->GetComponent<Transform>()->SetPosition(player_pos.x - 1, player_pos.y, player_pos.z);
				}
				//else if (Move_Storage[3-i] == 2) {
				else if (Move_Storage[i] == 2) {
					player->GetComponent<Transform>()->SetPosition(player_pos.x + 1, player_pos.y, player_pos.z);
				}
				//else if (Move_Storage[3-i] == 3) {
				else if (Move_Storage[i] == 3) {
					player->GetComponent<Transform>()->SetPosition(player_pos.x, player_pos.y, player_pos.z - 1);
				}
				player_pos = player->GetComponent<Transform>()->GetPosition();
			}*/

			//プレイヤーの移動
			//上
			if (Move_Storage[moveflgCount] == 0) {
				auto act = player->GetComponent<Action>();
				act->AddMoveBy(0.5f, Vector3(0, 0, 1.0));
			}
			//左
			//else if (Move_Storage[3-i] == 1) {
			else if (Move_Storage[moveflgCount] == 1) {
				auto act = player->GetComponent<Action>();
				act->AddMoveBy(0.5f, Vector3(-1.0, 0, 0));
			}
			//右
			//else if (Move_Storage[3-i] == 2) {
			else if (Move_Storage[moveflgCount] == 2) {
				auto act = player->GetComponent<Action>();
				act->AddMoveBy(0.5f, Vector3(1.0, 0, 0));
			}
			//下
			//else if (Move_Storage[3-i] == 3) {
			else if (Move_Storage[moveflgCount] == 3) {
				auto act = player->GetComponent<Action>();
				act->AddMoveBy(0.5f, Vector3(0, 0, -1.0));
			}

			else if (Move_Storage[moveflgCount] == 4) {
				auto act = player->GetComponent<Action>();
				act->AddMoveBy(0.5f, Vector3(0, 0, -2.0));
			}
			for (int i = 0; i < Select->size(); i++) {
				Select->at(i)->SetDrawActive(true);

			}
			GetSharedGameObject<WallSprite>(L"Mainplate")->SetDrawActive(true);
			moveflgCount++;
			if (moveflgCount >= 3)
			{
				auto act = player->GetComponent<Action>();
				act->Run();

				/*			if (act->IsArrived()) {
							act->AllActionClear();
							}*/
				for (int i = 0; i < 3; i++) {
					Move_Storage[i] = NULL;
				}
				moveflgCount = 0;
				move_flg = false;

				//moveTimer++;
				//if (moveTimer > 360.0f) {
				//	move_flg = false;
				//}
			}
		}
	}
}
//end basecross
