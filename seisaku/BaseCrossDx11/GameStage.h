/*!
@file GameStage.h
@brief ゲームステージ
*/

#pragma once
#include "stdafx.h"

namespace basecross {

	//--------------------------------------------------------------------------------------
	//	ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage {
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViewLight();
		//プレートの作成
		void CreatePlate();
		//追いかけるオブジェクトの作成
		void CreateSeekObject();
		//固定のボックスの作成
		void CreateFixedBox();
		//上下移動しているボックスの作成
		void CreateMoveBox();
		//ヒットする球体の作成
		void CreateSphere();
		//半透明のスプライト作成
		/*void CreateTraceSprite();*/
		//壁模様のスプライト作成
		void CreateWallSprite();
		////スクロールするスプライト作成
		//void CreateScrollSprite();

		//ゴールのボックスの作成
		void CreateGoalBox();

		//スパークの作成
		void CreateSpark();
		//プレイヤーの作成
		void CreatePlayer();
		//選択肢のカーソル
		void SelectPlateControl();


	public:
		//せれくとぷれーと選択用の変数
		int selectNum = 0;
		//セレクトプレートのアクション的な何か
		bool flgaaaaaa = false;

		//行動を格納する金内
		int Move_Storage[3] = { 0 };

		//現在Move_Storageにいくつ格納したか数えるカウンター金内
		int MS_Counter = 0;

		//移動中かどうかのフラグ金内
		bool move_flg = false;

		int moveflgCount = 0;
		int moveTimer = 0;

		//構築と破棄
		GameStage() :Stage() {}
		virtual ~GameStage() {}
		//初期化
		virtual void OnCreate()override;
		virtual void OnUpdate()override;
	};


}
//end basecross

